const responseMiddleware = (req, res, next) => {
  
  if (res.data) {
    res.status(200).json(res.data);
  } 
  
  if (res.err) {
    res.status(res.err.httpStatusCode).json({error: true, message: res.err.message});
  }
  next();
}


exports.responseMiddleware = responseMiddleware;