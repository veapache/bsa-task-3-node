const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
  try {
    const { name, defense, power } = req.body;

    if (!name || !defense || !power) {
      throw new validationError('Missing name or defense or power');
    }
    const allFighters = FighterService.getFighters();
    for (let i = 0; i < allFighters.length; i++) {
      if (allFighters[i].name.toLowerCase() === name.toLowerCase()){
        throw new validationError('Fighter with name exists');
      }
    }

    if (!validateFighterDefense(defense)) {
      throw new validationError('Fighter defense not between 1 to 10');
    } 
    if (!validateFighterPower(power)) {
      throw new validationError('Fighter power is more than 100');
    } 
    const fighter = {
      name: name,
      health: 100,
      defense: defense,
      power: power
    }
    res.validationData = fighter;
  } catch (error) {
    res.err = error;
  }
  next();
}

const updateFighterValid = (req, res, next) => {
  try {
    const { name, defense, power } = req.body;
    const fighetrId = req.params.id;
    const currentFighter = FighterService.searchFighter({id: fighetrId});

    if (!currentFighter) {
      throw new validationError('Fighter not found');
    }
    // if (!name || !defense || !power) {
    //   throw new validationError('Missing name or defense or power');
    // }
    if (name) {
      const allFighters = FighterService.getFighters();
      for (let i = 0; i < allFighters.length; i++) {
        if (allFighters[i].name.toLowerCase() === name.toLowerCase()){
          throw new validationError('Fighter with name exists');
        }
      }
    }

    if (defense && !validateFighterDefense(defense)) {
      throw new validationError('Fighter defense not between 1 to 10');
    } 
    
    if (power && !validateFighterPower(power)) {
      throw new validationError('Fighter power is more than 100');
    } 
    const fighter = {
      name: name || currentFighter.name,
      health: 100 || currentFighter.health,
      defense: defense || currentFighter.defense,
      power: power || currentFighter.power
    }
    res.validationData = fighter;
  } catch (error) {
    res.err = error;
  }
  next();
}

const getFighetrValid = (req, res, next) => {
  try {
    const fighetrId = req.params.id;
    const currentFighetr = FighterService.searchFighter({id: fighetrId});
    if (!currentFighetr) {
      throw new searchError('Fighter not found')
    }
  } catch (error) {
    res.err = error;
  }
  next();
}

function validationError(message) {
    const error = new Error(message);
    error.httpStatusCode = 400;
    return error;
}

function searchError(message) {
  const error = new Error(message);
  error.httpStatusCode = 404;
  return error;
}

function validateFighterExists(fighterName) {
  const fighter = FighterService.searchFighter({name: fighterName});
  return fighter || null;
}

function validateFighterPower(fighterPower) {
    return fighterPower < 100 && fighterPower > 0;
}

function validateFighterDefense(fighterDefense) {
    return fighterDefense > 1 && fighterDefense <= 10;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.getFighetrValid = getFighetrValid;