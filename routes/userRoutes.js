const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, getUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try{
    res.data = UserService.getUsers();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get("/:id", getUserValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = UserService.search({id: req.params.id});
    } catch (err) {
      res.err = err;
    }
  }
  next();
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = UserService.createUser(res.validationData);
    } catch (err) {
      res.err = err;
    }
  }
  next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = UserService.updateUser(req.params.id, res.validationData);
    } catch (err) {
      res.err = err;
    }
  }
  next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  // if (!res.err) {
    try {
      res.data = UserService.deleteUser(req.params.id);
    } catch (err) {
      res.err = err;
    }
  // }
  next();
}, responseMiddleware);


module.exports = router;