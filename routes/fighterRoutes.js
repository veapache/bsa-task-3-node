const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid, getFighetrValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
    res.data = FighterService.getFighters();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get("/:id", getFighetrValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = FighterService.searchFighter({id: req.params.id});
    } catch (err) {
      res.err = err;
    } 
  }
  next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = FighterService.createFighter(res.validationData);
    } catch (err) {
      res.err = err;
    }
  }
  next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  if (!res.err) {
    try {
      res.data = FighterService.updateFighter(req.params.id, res.validationData);
    } catch (err) {
      res.err = err;
    }
  }
  next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  // if(!res.err) {
    try {
      res.data = FighterService.deleteFighter(req.params.id);
    } catch (err) {
      res.err = err;
    }
  // }
  next();
}, responseMiddleware);

module.exports = router;