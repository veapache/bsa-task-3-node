const { UserRepository } = require('../repositories/userRepository');

class UserService {
  getUsers() {
    const items = UserRepository.getAll();
      if(!items) {
          return null;
      }
      return items; 
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }

  createUser(data) {
    const item = UserRepository.create(data);
    if(!item) {
      return null;
    }
    return item;
  }

  updateUser(id, data) {
    const item = UserRepository.update(id, data);
    if(!item) {
      return null;
    }
    return item;  
  }

  deleteUser(id) {
    const item = UserRepository.delete(id);
    if(!item) {
      return null;
    }
    return item;  
  }
}

module.exports = new UserService();