const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getFighters() {
    const items = FighterRepository.getAll();
    if(!items) {
      return null;
    }
    return items; 
  }
    
  searchFighter(search) {
    const item = FighterRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
    
  createFighter(data) {
    const item = FighterRepository.create(data);
    if(!item) {
      return null;
    }
    return item;
  }
    
  updateFighter(id, data) {
    const item = FighterRepository.update(id, data);
    if(!item) {
      return null;
    }
    return item;  
  }
    
  deleteFighter(id) {
    const item = FighterRepository.delete(id);
    if(!item) {
      return null;
    }
    return item;  
  }
}

module.exports = new FighterService();